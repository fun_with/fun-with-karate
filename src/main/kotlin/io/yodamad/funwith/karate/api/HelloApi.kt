package io.yodamad.funwith.karate.api

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api")
class HelloApi {

    @GetMapping
    fun world() = "{ \"type\" : \"message\", \"content\" : \"Hello world !\"}"
}