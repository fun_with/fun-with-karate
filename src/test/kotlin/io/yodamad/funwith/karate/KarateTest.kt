package io.yodamad.funwith.karate

import com.intuit.karate.junit5.Karate
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class KarateTest {

    @Karate.Test
    fun testAutoAll(): Karate {
        // It executes features that exist `only` in the package area.
        // ex) karate/deom/*.feature
        return Karate().path("classpath:/io/yodamad/funwith/karate")
    }
}