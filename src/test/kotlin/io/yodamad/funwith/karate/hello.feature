Feature: tests

  Background:
    * url baseURL

  Scenario: Hello
    Given path '/api'
    When method get
    Then status 200
    And match response == { "type" : "message", "content" : "Hello world !" }
    And match responseType == 'json'

  Scenario: KO
    Given path '/api/ko'
    When method get
    Then status 404